#!/usr/bin/env python

"""
Author: Wasin Poncheewin
Script to extract OTU sequence 
"""

from sys import argv
import re

if __name__ == "__main__":

	input_file = argv[1]
	out_location = argv[2]

	with open(input_file) as biomFile:
		biomFile_info = biomFile.read()

	#with open("output/mapping_file_test_emptyempty.biom") as biomFile:
	#	biomFile_info = biomFile.read()

	otuInfo_pattern = re.compile(r'\"id\": \"(.*)\",[\n][" "]*\"forwardSequence\": \"(.*)\",[\n][" "]*\"reverseSequence\": \"(.*)\"')

	match = re.findall(otuInfo_pattern, biomFile_info)

	print (len(match))

	otu_f = open(out_location+'otu_f.fasta', 'w+')
	otu_r = open(out_location+'otu_r.fasta', 'w+')
	otu_m = open(out_location+'otu_m.fasta', 'w+')

	for m in match:
		print (m)
		otu_f.write('>' + m[0] + '_f' + '\n')
		otu_f.write(m[1] + '\n')

		otu_r.write('>' + m[0] + '_r' + '\n')
		otu_r.write(m[2] + '\n')

		otu_m.write('>' + m[0] + '_m' + '\n')
		otu_m.write(m[1] + m[2] + '\n')

	otu_f.close()
	otu_r.close()
	otu_m.close()
