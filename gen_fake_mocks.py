#!/usr/bin/python3

import sys,getopt
import argparse
import os
import math
import pandas as pd
from pyfaidx import Fasta

def parse_options():
	usage = "\ngen_fake_mocks.py -f <dna.fasta,dna.fasta,..> -p mock_proportions_file -b mock_barcode_file -o output_name -n number_of_reads "
	parser = argparse.ArgumentParser(usage=usage, description='Generate fake mock community reads from fasta files')
	
	input_group = parser.add_argument_group('Required arguments')
	input_group.add_argument("-f","--fasta",  dest="fasta_filepath", help="DNA (multi)fasta file", required=True, metavar="FASTA")
	input_group.add_argument("-o","--output",  dest="output_location", help="output_location")
	input_group.add_argument("-p","--proportions",  dest="proportions_file", help="proportions file, see readme for the correct format", required=True)
	input_group.add_argument("-b","--barcodes",  dest="mock_barcodes_file", help="mock barcode files, see readme for correct format", required=True)
	input_group.add_argument("-n","--num_reads",  dest="num_reads", help="aproximate otal number of reads in the sample", required=True)

	inputs = parser.parse_args()

	return inputs

def main():

	# TODO: proper error handling....

	inputs = parse_options()

	fasta_files = inputs.fasta_filepath.split(",")
	mock_barcodes_file = inputs.mock_barcodes_file
	proportions_file = inputs.proportions_file
	total_nr_reads = int(inputs.num_reads)
	output_location = inputs.output_location

	# This should be an option
	readtype = "both"

	seqs = {}
	for fasta_file in fasta_files:
		fasta_name = os.path.split(fasta_file)[-1].replace(".fasta","").replace(".fa","").replace(".fna","")
		seqs[fasta_name] = Fasta(fasta_file, read_long_names=True,read_ahead=1000)

	
	proportions = pd.read_csv(proportions_file,index_col=0).fillna(0)

	# read mock barcodes into dictionary
	mock_barcodes = dict([line.strip().split() for line in open(mock_barcodes_file,"r").readlines()])

	for mock in proportions:

		print("\nGenerating files for", mock)

		if readtype == "fastq":
			mockfile_fastq_fwd = open(output_location+"/"+mock+"_1.fq","w")
			mockfile_fastq_rev = open(output_location+"/"+mock+"_2.fq","w")
		elif readtype == "both":
			mockfile_fastq_fwd = open(output_location+"/"+mock+"_1.fq","w")
			mockfile_fastq_rev = open(output_location+"/"+mock+"_2.fq","w")

			mockfile_fasta_fwd = open(output_location+"/"+mock+"_1.fa","w")
			mockfile_fasta_rev = open(output_location+"/"+mock+"_2.fa","w")
		elif readtype == "fasta":
			mockfile_fasta_fwd = open(output_location+"/"+mock+"_1.fa","w")
			mockfile_fasta_rev = open(output_location+"/"+mock+"_2.fa","w")


		stats = {}
		total_readcount = 0
		for strain in proportions.index:
			total_strain_readcount = 0
			strain_proportion = proportions[mock][strain]
			nr_strain_reads = total_nr_reads/2*strain_proportion

			barcode = mock_barcodes[mock]

			# WRITE READS
			x = math.ceil(nr_strain_reads/len(seqs[strain].keys()))
			for i in range(x):
				strain_readnr = str(i+1)
				for entry in seqs[strain]:
					total_strain_readcount += 2
					total_readcount += 2

					quality_line = "J"*len(entry[:])
					if readtype == "fastq":
						fastq_fwd_read = "@"+entry.name.strip()+"_"+strain_readnr.zfill(6)+"-"+str(total_strain_readcount-1).zfill(6)+"/1\n"+barcode+str(entry[:])+"\n+\n"+quality_line+"\n"
						fastq_rev_read = "@"+entry.name.strip()+"_"+strain_readnr.zfill(6)+"-"+str(total_strain_readcount).zfill(6)+"/2\n"+barcode+str(-entry[:])+"\n+\n"+quality_line+"\n"

						mockfile_fastq_fwd.write(fastq_fwd_read)
						mockfile_fastq_rev.write(fastq_rev_read)

					elif readtype == "both":
						fastq_fwd_read = "@"+entry.name.strip()+"_"+strain_readnr.zfill(6)+"-"+str(total_strain_readcount-1).zfill(6)+"/1\n"+barcode+str(entry[:])+"\n+\n"+quality_line+"\n"
						fastq_rev_read = "@"+entry.name.strip()+"_"+strain_readnr.zfill(6)+"-"+str(total_strain_readcount).zfill(6)+"/2\n"+barcode+str(-entry[:])+"\n+\n"+quality_line+"\n"

						mockfile_fastq_fwd.write(fastq_fwd_read)
						mockfile_fastq_rev.write(fastq_rev_read)

						fasta_fwd_read = ">fwd_"+entry.name.strip()+"_"+strain_readnr.zfill(6)+"-"+str(total_strain_readcount-1).zfill(6)+"\n"+barcode+str(entry[:])+"\n"
						fasta_rev_read = ">rev_"+entry.name.strip()+"_"+strain_readnr.zfill(6)+"-"+str(total_strain_readcount).zfill(6)+"\n"+barcode+str(-entry[:])+"\n"

						mockfile_fasta_fwd.write(fasta_fwd_read)
						mockfile_fasta_rev.write(fasta_rev_read)

					elif readtype == "fasta":
						fasta_fwd_read = ">fwd_"+entry.name.strip()+"_"+strain_readnr.zfill(6)+"-"+str(total_readcount-1).zfill(6)+"\n"+barcode+str(entry[:])+"\n"
						fasta_rev_read = ">rev_"+entry.name.strip()+"_"+strain_readnr.zfill(6)+"-"+str(total_readcount).zfill(6)+"\n"+barcode+str(-entry[:])+"\n"

						mockfile_fasta_fwd.write(fasta_fwd_read)
						mockfile_fasta_rev.write(fasta_rev_read)

			stats[strain] = total_strain_readcount

		if readtype == "fastq":
			mockfile_fastq_fwd.close()
			mockfile_fastq_rev.close()
		elif readtype == "both":
			mockfile_fastq_fwd.close()
			mockfile_fastq_rev.close()
			mockfile_fasta_fwd.close()
			mockfile_fasta_rev.close()
		elif readtype == "fasta":
			mockfile_fasta_fwd.close()
			mockfile_fasta_rev.close()

		# std out stats (nr of reads etc..)
		total_mock_reads = sum(stats.values())
		print("\n"+mock+" ("+str(total_readcount)+" reads)")
		for strain in stats:
			print(strain+"\t"+str(len(seqs[strain].keys()))+"\t"+str(stats[strain])+"\t"+str(stats[strain]/total_mock_reads)+"\t"+str(proportions[mock][strain]))

if __name__ == "__main__":
	main()
