<h4>Generate theoretical mock community paired-end data with predefined proportions from fasta sequences.</h4>

It uses the complete sequences for the forward and reverse generation. 

Output is fastq and fasta.

Requires: pandas and pyfaidx

See paper_data folder for the the format of the proportions and barcode files.

Example usage (only test on ubuntu 18.04):

python3 gen_fake_mocks.py --fasta paper_data/28xy.fasta,paper_data/CaDo13a.fasta,paper_data/CaDo16a.fasta,paper_data/RE1.fasta,paper_data/SR2.fasta --proportions paper_data/mock_proportions.txt --barcodes paper_data/mock_barcodes.txt --num_reads 200000 --output paper_data/output 


Standard Output (see paper_data/T_Mock_stats.txt):

Generating files for T_Mock_1

T_Mock_1 (200002 reads)<br />
RE1	17	98498	0.4924850751492485	0.49248546200000004<br />
SR2	19	0	0.0	0.0<br />
CaDo16a	19	0	0.0	0.0<br />
CaDo13a	14	0	0.0	0.0<br />
28xy	16	101504	0.5075149248507514	0.507514538<br />

Generating files for T_Mock_2

T_Mock_2 (200054 reads)<br />
RE1	17	37638	0.18813920241534784	0.18813807300000002<br />
SR2	19	43130	0.21559179021664152	0.21558613100000001<br />
CaDo16a	19	40014	0.2000159956811661	0.200045545<br />
CaDo13a	14	40488	0.20238535595389245	0.20235080800000002<br />
28xy	16	38784	0.19386765573295212	0.193879443<br />

Generating files for T_Mock_3

T_Mock_3 (200132 reads)<br />
RE1	17	95472	0.4770451502008674	0.477211909<br />
SR2	19	43776	0.2187356344812424	0.21873354600000003<br />
CaDo16a	19	30476	0.15227949553294826	0.152224558<br />
CaDo13a	14	20552	0.1026922231327324	0.10265249800000001<br />
28xy	16	9856	0.049247496652209545	0.049177489000000005<br />

Generating files for T_Mock_4<br />

T_Mock_4 (200048 reads)<br />
RE1	17	176324	0.8814084619691274	0.881551148<br />
SR2	19	228	0.0011397264656482443	0.00113642<br />
CaDo16a	19	2128	0.010637447012716949	0.010545005<br />
CaDo13a	14	21336	0.1066544029432936	0.10666522699999999<br />
28xy	16	32	0.00015996160921378868	0.0001022<br />
